import numpy as np
import cv2

class BoxDetector:
    def __init__(self, kernal=17, thresh=40, minArea=100):
        self.kernal = kernal
        self.thresh = thresh
        self.minArea = minArea
        self.isV4 = cv2.__version__.startswith('4.')

    def _area(self, shape):
        '''Return the area of a polygon'''
        p = np.append(shape, [shape[0]], axis=0)
        a = 0
        for i in range(p.shape[0] - 1):
            a += (p[i][0] * p[i+1][1]) - (p[i+1][0] * p[i][1])
        return abs(a / 2)
    
    def detect(self, img):
        '''Detects if a black box is present'''
        img = img.astype('uint8')
        blurred = cv2.GaussianBlur(img, (self.kernal, self.kernal), 0)
        thresh = cv2.threshold(blurred, self.thresh,
                               255, cv2.THRESH_BINARY_INV)[1]
        # find top of barrier
        top = 0
        for row in thresh:
            if (row == 0).all():
                break
            top += 1
        cropped = thresh[top:, :]
        countours = None
        if self.isV4:
            contours, _ = cv2.findContours(cropped, cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_SIMPLE)
        else:
            _, contours, _ = cv2.findContours(cropped, cv2.RETR_EXTERNAL,
                                              cv2.CHAIN_APPROX_SIMPLE)

        for c in contours:
            rect = cv2.minAreaRect(c)
            box = cv2.boxPoints(rect)
            if self._area(box) >= self.minArea:
                return True
        return False
