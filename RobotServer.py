from xmlrpc.server import SimpleXMLRPCServer
import hardware

motors = hardware.Motors()
sensors = hardware.Sensors()
camera = iter(hardware.Camera())

def motors_set(left, right, duration):
    motors.set(left, right, duration)
    return True

def motors_stop():
    motors.stop()
    return True

def sensors_read():
    return sensors.read().tobytes()

def camera_next():
    return next(camera).tobytes()

server = SimpleXMLRPCServer(("", 8000))
print("Listening on port 8000...")
server.register_function(motors_set, "motors_set")

server.register_function(motors_stop, "motors_stop")

server.register_function(sensors_read, "sensors_read")

server.register_function(camera_next, "camera_next")

server.serve_forever()
