# RemoteHardware.py
# Defines remote interfaces for motors, sensors, and camera.

import numpy as np
import xmlrpc.client

remote = xmlrpc.client.ServerProxy("http://raspberrypi:8000/")

class Motors:
    def __init__(self, pwmFreq = 50):
        pass

    def set(self, left, right, duration=None):
        '''Sets motors to values (l, r), where l and r are floats
        between -1 and 1'''
        if left < -1 or left > 1 or right < -1 or right > 1:
            raise ValueError("motor values must be between -1 and 1")
        remote.motors_set(float(left), float(right), duration)

    def stop(self):
        '''Stops all motors'''
        remote.motors_stop()

class Sensors:
    def read(self):
        '''Return a list of 3 sensor values in CM [left, middle, right]'''
        return np.frombuffer(remote.sensors_read().data)

class Camera:
    def __iter__(self):
        return self

    def __next__(self):
        return np.frombuffer(remote.camera_next().data,
                             dtype='uint8').reshape((480, 640))
