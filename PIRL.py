# PIRL.py
# Implements PIRL program synthesizer

from itertools import permutations, chain, product
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import time, math, os, sys, pickle
from hyperopt import fmin, tpe, hp

class PIRLAgent:
    def __init__(self, DRL, sketch, delta = 20.0):
        self.robot = DRL.robot
        self.delta = delta
        self.DRL = DRL
        self.history = []
        self.program = None
        self.limits = None
        self.synth = SketchSynth(sketch, self._error)

    def save(self, folder):
        '''Saves the current program and history'''
        with open(os.path.join(folder, "program.pkl"), 'wb') as output:
            pickle.dump((self.program, self.limits), output, 
                        pickle.HIGHEST_PROTOCOL)
        with open(os.path.join(folder, "history.pkl"), 'wb') as output:
            pickle.dump(self.history, output, pickle.HIGHEST_PROTOCOL)

    def load(self, folder):
        '''Attempts to load saved history and program from files'''
        program_file = os.path.join(folder, "program.pkl")
        history_file = os.path.join(folder, "history.pkl")
        if not (os.path.exists(program_file) and os.path.exists(history_file)):
            return False
        with open(os.path.join(folder, "program.pkl"), 'rb') as input:
            self.program, self.limits = pickle.load(input)
        with open(os.path.join(folder, "history.pkl"), 'rb') as input:
            self.history = pickle.load(input)
        return True

    def get_string(self, program):
        '''Gets a string representation of the given program'''
        return self.synth.print_program(program)

    def create_histories(self):
        '''Initializes the histories set'''
        _, history = self.DRL.run_episode(False)
        self.history += history

    def init_program(self, size):
        '''Finds initial program by searching all programs of size'''
        self.program = self.synth.get_best_program(size)
        self.limits = np.array([op.size for op in self.program])

    def run_episode(self, learn = True):
        '''Runs a single episode and returns cumulative reward'''
        reward_steps = 0
        episode_reward = 0
        episode_inputs = []
        for step in range(50):
            # get current policy decisions
            inputs = self.robot.get_inputs()
            episode_inputs.append(inputs)
            motor_vals = np.clip(
                    self.synth.run_program((self.program, {}), inputs),
                    -1.0, 1.0)
            # evaluate the policy
            # original paper used 7ms, I think its too short
            self.robot.motors.set(*motor_vals, 0.05)

            box_visible = self.robot.is_box_visible()
            # The robot is rewarded if it's pushing the box
            if (self.robot.is_pushing_box() and (motor_vals > 0).all()
                and box_visible):
                episode_reward += 0.018
                reward_steps += 1
            else:
                reward_steps = 0
            # finish episode if box is no longer visible or if 10 reward steps
            if not box_visible or reward_steps == 10:
                break

        if learn:
            # update histories
            self._update_history(episode_inputs)

        # return None for history to make compatable with DRL agent
        return episode_reward, None

    def update_program(self):
        '''Updates the program to one that is the best in the neighborhood of
        the current program'''
        self.limits += 1
        self.program = self.synth.get_best_nearby_program(self.program,
                                                          self.limits)
        return self.program

    def _update_history(self, ep_inputs):
        for inputs in ep_inputs:
            should_add = True
            for h in self.history:
                if np.linalg.norm(inputs - h[0]) < self.delta:
                    should_add = False
                    break
            if should_add:
                outputs = self.DRL.get_outputs(inputs)
                self.history.append((inputs, outputs))

    def _error(self, program):
        '''Compute the error between the DRL agent and the current program'''
        err = 0
        for h in self.history:
            inputs = h[0]
            outputs = h[1]
            run = self.synth.run_program(program, inputs)
            if np.isnan(run).any():
                return np.inf
            err += np.linalg.norm(run - outputs)
        return err 

class SketchSynth:
    def __init__(self, sketch, error_func):
        # sketch is string with valid python code contianing holes
        self.sketch = sketch
        self.error_func = error_func
        self.holes = sketch.count("{}")
        
    def get_best_program(self, size):
        '''Returns best program of size'''
        if size < self.holes:
            raise ValueError("Minimum program size for sketch is {}"
                    .format(self.holes))
        return self._best_program(self._enumerate_n_atoms(self.holes, size))

    def get_best_nearby_program(self, program, limits):
        '''Returns best program in neighborhood of current program'''
        # generate neighborhood programs
        n_programs = []
        for i in range(self.holes):
            p = list(program)
            for atom in self._enumerate_atoms(limits[i]):
                p[i] = atom
                n_programs.append(tuple(p))
        return self._best_program(n_programs)

    def run_program(self, program, inputs):
        '''Evaluate a program'''
        try:
            return eval(self.sketch.format(*map(str, program[0])), 
                    {"np":np, "inputs":inputs, **program[1]})
        except (ValueError, IndexError) as _: 
            return np.nan

    def print_program(self, program):
        return self.sketch.format(*map(str, program))

    def _best_program(self, programs):
        program = None
        program_error = math.inf
        none_consts = lambda op: isinstance(op, Constant) and op.value == None
        for holes in programs:
            # optimize constants
            consts = {}
            space = {}
            for hole in holes:
                for c in filter(none_consts, hole.ops()):
                    space = {**space, **c.search_space()}
            if len(space) > 0:
                consts = fmin(fn=lambda c: self.error_func((holes, c)),
                        space = space, algo=tpe.suggest, max_evals=30,
                        loss_threshold=1e-4, verbose=False)
            if self.error_func((holes, consts)) < program_error:
                program = (holes, consts)
        return self._bake_inputs(program)

    def _bake_inputs(self, program):
        none_consts = lambda op: isinstance(op, Constant) and op.value == None
        for hole in program[0]:
            for c in filter(none_consts, hole.ops()):
                c.value = program[1][c.name]
        return program[0]

    def _enumerate_atoms(self, size):
        if size <= 0:
            return []
        elif size == 1:
            # only constants are size 1
            return [FloatConstant()]
        ret = []
        for array in self._enumerate_arrays(size - 1):
            ret.append(Atom("max", array))
            ret.append(Atom("argmax", array))
            ret.append(Atom("min", array))
            ret.append(Atom("argmin", array))
            ret.append(Atom("ptp", array))
            ret.append(Atom("sum", array))
            ret.append(Atom("all", array))
            ret.append(Atom("any", array))
        for array in self._enumerate_arrays(size - 2):
            ret.append(Atom("slice", array, (IntConstant(),)))
        return ret
 
    def _enumerate_n_atoms(self, n, size):
        # Credit to falsetru on StackOverflow for following function
        def _part(n, k, pre):
            if n <= 0:
                return []
            if k == 1:
                if n <= pre:
                    return [[n]]
                return []
            ret = []
            for i in range(min(pre, n), 0, -1):
                ret += [[i] + sub for sub in _part(n-i, k-1, i)]
            return ret
        ret = []
        for sizes in set(chain(*map(permutations,_part(size, n, size)))):
            atoms = [self._enumerate_atoms(s) for s in sizes]
            ret += list(product(*atoms))
        return ret
       
    def _enumerate_arrays(self, size):
        if size <= 0:
            return []
        elif size == 1:
            #only one array that is singular, the inputs
            return [Inputs()]
        ret = []
        for i in range(1, int(math.floor(size / 2) + 1)):
            for array in self._enumerate_arrays(i - 1):
                for atom in self._enumerate_atoms(size - i):
                    ret.append(Array("__lt__", array, (atom,)))
                    ret.append(Array("__gt__", array, (atom,)))
                    ret.append(Array("__add__", array, (atom,)))
                    ret.append(Array("__mul__", array, (atom,)))
                # append slices
                if size - i == 2:
                    ret.append(Array("slice", array,
                                     (IntConstant(), IntConstant())))
                if size - i == 3:
                    ret.append(Array("slice", array,
                                     (IntConstant(), IntConstant(),
                                      IntConstant())))
            if i != size - i:
                for array in self._enumerate_arrays(size - i - 1):
                    for atom in self._enumerate_atoms(i):
                        ret.append(Array("__lt__", array, (atom,)))
                        ret.append(Array("__gt__", array, (atom,)))
                        ret.append(Array("__add__", array, (atom,)))
                        ret.append(Array("__mul__", array, (atom,)))
                    # append slices
                    if i == 2:
                        ret.append(Array("slice", array,
                                         (IntConstant(), IntConstant())))
                    if i == 3:
                        ret.append(Array("slice", array,
                                         (IntConstant(), IntConstant(),
                                          IntConstant())))
        return ret


class Atom:
    def __init__(self, op, arr, args=tuple()):
        self.op = op
        self.arr = arr
        self.args = args
        self.size = 1 + arr.size

    def ops(self):
        ret =  [self] + self.arr.ops()
        for arg in self.args:
            ret += arg.ops()
        return ret

    def __str__(self):
        if self.op == "slice":
            return str(self.arr) + "[int(" + str(self.args[0]) + ")]"
        return str(self.arr) + "." + self.op + "().astype('float32')"

class Constant(Atom):
    def __init__(self):
        self.size = 1
        self.value = None
        self.name = "const_" + str(hash(self))

    def ops(self):
        return [self]

    def __str__(self):
        if self.value is not None:
            return str(self.value)
        return self.name

class IntConstant(Constant):
    def search_space(self):
        # range of 0 to len(inputs) - 1
        return {self.name : hp.quniform(self.name, 0, 12291, 1)}
    
    def __str__(self):
        if self.value is not None:
            return str(int(self.value))
        return self.name

class FloatConstant(Constant):
    def search_space(self):
        return {self.name : hp.uniform(self.name, -1.0, 1.0)}
    
class Array:
    def __init__(self, op, arr, args):
        self.op = op
        self.arr = arr
        self.args = args
        self.size = 1 + self.arr.size
        for arg in self.args:
            self.size += arg.size

    def ops(self):
        ret = [self] + self.arr.ops()
        for arg in self.args:
            ret += arg.ops()
        return ret

    def __str__(self):
        if self.op == "slice":
            if len(self.args) == 2:
                return (str(self.arr) + "[int(" + str(self.args[0]) + "):int("
                        + str(self.args[1]) + ")]")
            else:
                return (str(self.arr) + "[int(" + str(self.args[0]) + "):int("
                        + str(self.args[1]) + "):int(" 
                        + str(self.args[2]) + ")]")
        return (str(self.arr) + "." + self.op + "(" + str(self.args[0])
                + ").astype('float32')")

class Inputs(Array):
    def __init__(self):
        self.size = 1

    def ops(self):
        return [self]

    def __str__(self):
        return "inputs"
