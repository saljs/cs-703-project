# Robot.py 
# Abstraction layer around hardware interface

try:
    import hardware
except:
    import RemoteHardware as hardware

import numpy as np
import cv2
from BoxDetector import BoxDetector

class Robot:
    def __init__(self):
        self.motors = hardware.Motors()
        self.sensors = hardware.Sensors()
        self.image = iter(hardware.Camera())
        self.detector = BoxDetector()

    def get_inputs(self):
        '''Gets a numpy array of inputs useful for machine-learning'''
        img = next(self.image)
        # take average of 5x5 pixel subarrays
        small_img = cv2.resize(img,
                               (int(img.shape[0] / 5), int(img.shape[1] / 5)),
                               cv2.INTER_NEAREST)
        # normalize and flatten image
        norm_img = small_img.flatten() / 255
        # normalize distance sensor readings
        norm_dist = np.clip(self.sensors.read() / 170, 0, 1)
        return np.concatenate((norm_dist, norm_img))

    def is_box_visible(self):
        '''Returns a boolean value indicating if the box is currently in the
        robot's field of view'''
        return self.detector.detect(next(self.image))
    
    def is_pushing_box(self):
        '''Returns a boolean value indicating if the robot is currently pushing
        the box'''
        if (self.sensors.read() == 0).any():
            # read sensors twice to get more accurate reading
            return (self.sensors.read() == 0).any()
        return False
