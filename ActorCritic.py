# ActorCritic.py
# Implements the original actor-critic DL model from the 2003 paper.

import tensorflow as tf
from tensorflow.keras.initializers import *
import numpy as np
import time

class ActorCriticAgent:
    def __init__(self, robot, lr=7e-3, gamma=0.99):
        self.robot = robot
        self.gamma = gamma
        # Build neural network
        input_layer = tf.keras.Input(shape=(12291,))
        hidden1 = tf.keras.layers.Dense(1024,
                kernel_initializer=RandomUniform(minval=-0.1, maxval=0.1),
                bias_initializer=RandomUniform(minval=-0.1, maxval=0.1),
                activation='relu')(input_layer)
        hidden2 = tf.keras.layers.Dense(128,
                kernel_initializer=Zeros(),
                bias_initializer=Zeros(),
                activation='relu')(hidden1)
        actor_layer = tf.keras.layers.Dense(2,
                kernel_initializer=Zeros(),
                bias_initializer=Zeros(),
                activation='tanh')(hidden2)
        critic_layer = tf.keras.layers.Dense(1,
                kernel_initializer=Zeros(),
                bias_initializer=Zeros(),
                activation='tanh')(hidden2)
        self.model = tf.keras.Model(inputs=input_layer,
                                    outputs=[actor_layer, critic_layer])
        self.model.compile(
                optimizer=tf.keras.optimizers.RMSprop(lr=lr),
                loss="mse")

    def run_episode(self, learn = True):
        '''Runs a single episode. Returns the cumalative reward and history. If
        learn is set to false, no backpropagation step will be run'''
        reward_steps = 0
        episode_reward = []
        episode_actor_vals = []
        episode_critic_vals = []
        episode_inputs = []
        for step in range(50):
            # get current policy decisions
            inputs, actor_vals, critic_val = self._get_values()
            episode_inputs.append(inputs)
            episode_actor_vals.append(actor_vals)
            episode_critic_vals.append(critic_val)

            # evaluate the policy
            # original paper used 7ms, I think its too short
            self.robot.motors.set(*actor_vals, 0.05)

            box_visible = self.robot.is_box_visible()
            # The robot is rewarded if it's pushing the box
            if (self.robot.is_pushing_box() and (actor_vals > 0).all()
                and box_visible):
                episode_reward.append(0.018)
                reward_steps += 1
            else:
                episode_reward.append(0.0)
                reward_steps = 0
            # finish episode if box is no longer visible or if 10 reward steps
            if not box_visible or reward_steps == 10:
                break

        # append current predictions
        _, actor_vals, critic_val = self._get_values()
        episode_actor_vals.append(actor_vals)
        episode_critic_vals.append(critic_val)
        episode_reward.append(0.0)
    
        episode_inputs = np.array(episode_inputs)
        episode_reward = np.array(episode_reward)
        episode_actor_vals = np.array(episode_actor_vals)
        episode_critic_vals = np.array(episode_critic_vals)
        
        if learn:
            # get errors
            actor_err, critic_err = self._get_TD_errors(episode_reward, 
                                                        episode_actor_vals,
                                                        episode_critic_vals)
            # backpropogate errors
            losses = self.model.train_on_batch(episode_inputs,
                                               [actor_err, critic_err])
        
        return episode_reward.sum(), list(zip(episode_inputs,
                                              episode_actor_vals))

    def get_outputs(self, inputs):
        '''Get motor values given input array'''
        actor_vals, _ = self.model.predict(inputs[None,:])
        return actor_vals

    def _get_values(self):
        inputs = self.robot.get_inputs()
        actor_vals, critic_val = self.model.predict(inputs[None,:])
        # add 0.5 to critic
        critic_val += 0.5
        # add random number to actors
        actor_vals += np.random.uniform(low=0, high=0.15)
        actor_vals = np.clip(actor_vals, -1.0, 1.0)
        return inputs, np.squeeze(actor_vals), np.squeeze(critic_val)
    
    def _get_TD_errors(self, reward, actor, critic):
        # Not the standard TD error, but this is what the paper specifies
        critic_err = np.empty(critic.shape[0] - 1)
        actor_err = np.empty((actor.shape[0] - 1, 2))
        for t in range(reward.shape[0] - 1):
            critic_err[t] = reward[t+1] + self.gamma * critic[t+1]
            actor_err[t,:] = (actor[t] + reward[t+1] + self.gamma * critic[t+1]
                              - critic[t])
        return actor_err, critic_err
