## Programmatically Interpretable Reinforcement Learning in a Robotics Domain

This is my research project for CS 703, Program Verification and Synthesis, at the Univeristy of Wisconsin - Madison. It applies [Programmatically Interpretable Reinforcement Learning](https://arxiv.org/pdf/1804.02477.pdf) (PIRL) to a robotics experiment modeled after [this one](http://shws.cc.oita-u.ac.jp/~shibata/pub/SICE03.pdf). This repository contains all the necessary details and code to reproduce the experiments I ran for this research.

### Constructing the robot
To construct the robot used in these experiments, the following components are
needed:

* A Raspberry Pi 3/4 Model B
* [PCA9685 motor driver hat](https://www.amazon.com/gp/product/B07K7NP7C9/ref=ppx_yo_dt_b_asin_title_o06_s00)
* 3 [RCWL-1601 distance sensors](https://www.adafruit.com/product/4007)
* 2 [Motors and wheels](https://www.amazon.com/Electric-Magnetic-Gearbox-Plastic-Yeeco/dp/B07DQGX369/ref=sr_1_10)
* [Raspberry Pi Camera Module](https://www.amazon.com/gp/product/B012V1HEP4/ref=ppx_yo_dt_b_asin_title_o08_s00)
* A 12v 3A power supply
* 16 1.6mm machine screws
* 4 1.6mm nuts
* 4 2.5mm machine screws

You will also need to compile and 3d print [this](hardware/robot.scad) model.
The correct pins to connect the sensors to are listed in [hardware.py](hardare.py).

### Running the code
There are two ways to run the code:

1. Launch [RobotServer.py](RobotServer.py) on the Raspberry Pi and run [main.py](main.py) on a different computer on the same network for remote control of the robot.
1. Run [main.py](main.py) directly on the robot.

The Deep Reinforcement Learning agent needs to be trained first, using the `drl` subcommand. After this agent has been trained, a program can be synthesized using PIRL by using the `pirl` subcommand. Testing of both RL agents can be done with the `test` subcommand. 

### Examples
[Example of the DRL agent](https://youtu.be/lhxO0_aBv6Q)

[Example of the PIRL agent](https://youtu.be/1ajbrXXjhew)
