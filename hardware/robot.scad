$fn=50;

include <threads.scad>;
DEVEL=false;
length=104;
height=22;
edge_offset = sin(67.5) * (length / 2);

motor=[70.5, 19, 22];

pi_offset = 13;
pi_width = 49;
pi_length = 58;

sensor=[37, 15];

camera=[25, 24, 5.6];
camera_angle = 15;
camera_height = 10 / sin(90 - camera_angle) - 10.25;

stabalizer_r = 10.5;

difference() {
    union() {
        //body
        rotate([0, 0, 22.5])
            cylinder(d=length, h=height, $fn=8);
        
        //camera mounts
        translate([edge_offset - camera[2], -(camera[0]/2), height])
            rotate([0, camera_angle, 0])
                cube([3, 4, camera[1] + camera_height]);
        translate([edge_offset - camera[2], (camera[0]/2) - 4, height])
            rotate([0, camera_angle, 0])
                cube([3, 4, camera[1] + camera_height]);
        
        //pi mounting offsets
        translate([edge_offset - pi_offset, 0, height]) {
            translate([0, (pi_width/2), 0])
                cylinder(d=6, h=4);
            translate([0, -(pi_width/2), 0])
                cylinder(d=6, h=4);
            translate([-pi_length, (pi_width/2), 0])
                cylinder(d=6, h=4);
            translate([-pi_length, -(pi_width/2), 0])
                cylinder(d=6, h=4);
        }
        
        //sensor mounting offsets
        for(angle = [-45, 0, 45]) {
            //translate([0, (sensor[1]/2), height - 1.5])
            rotate([0, 90, angle]) {
                offset = angle == 0 ? 5 : 3;
                translate([-height + offset, (sensor[0]/2), edge_offset])
                    cylinder(d=3, h=4);
                translate([-height + offset, -(sensor[0]/2), edge_offset])
                    cylinder(d=3, h=4);
                translate([-height + offset + sensor[1], (sensor[0]/2), edge_offset])
                    cylinder(d=3, h=4);
                translate([-height + offset + sensor[1], -(sensor[0]/2), edge_offset])
                    cylinder(d=3, h=4);
            }
        }
    }
    union() {

        //camera mounting holes
        translate([edge_offset - camera[2], -(camera[0]/2), height])
            rotate([0, camera_angle, 0]) {
                translate([0, 2, 9.5])
                    rotate([0, 90, 0])
                        cylinder(d=1.8, h=3);
                translate([0, 2, 22])
                    rotate([0, 90, 0])
                        cylinder(d=1.8, h=3);
                translate([0, 23, 9.5])
                    rotate([0, 90, 0])
                        cylinder(d=1.8, h=3);
                translate([0, 23, 22])
                    rotate([0, 90, 0])
                        cylinder(d=1.8, h=3);
            }
        
        
        //motor mounts
        translate([-motor[0] + 18, 0, 0]) {
            translate([0, (length/2) - motor[1], 0])
                cube(motor);
            translate([0, -(length/2), 0])
                cube(motor);
            translate([motor[0] - 18, (length/2), motor[2]/2])
                rotate([90, 0, 0])
                    cylinder(d=6, h=length);
            
            translate([motor[0] - 37.7, (length/2) - motor[1], 2.3])
                rotate([90, 0, 0])
                    cylinder(d=3.4, h=16);
            translate([motor[0] - 40.4, (length/2) - motor[1] - 5, 0])
                cube([5.4, 2.6, 8.5]);
            translate([motor[0] - 37.7, -(length/2) + motor[1], 2.3])
                rotate([-90, 0, 0])
                    cylinder(d=3.4, h=16);
            translate([motor[0] - 40.4, -(length/2) + motor[1] + 2.4, 0])
                cube([5.4, 2.6, 8.5]);
            
            mirror([0, 0, 1]) {
                translate([0, 0, -height]) {
                    translate([motor[0] - 37.7, (length/2) - motor[1], 2.3])
                        rotate([90, 0, 0])
                            cylinder(d=3.4, h=16);
                    translate([motor[0] - 40.4, (length/2) - motor[1] - 5, 0])
                        cube([5.4, 2.6, 8.5]);
                    translate([motor[0] - 37.7, -(length/2) + motor[1], 2.3])
                        rotate([-90, 0, 0])
                            cylinder(d=3.4, h=16);
                    translate([motor[0] - 40.4, -(length/2) + motor[1] + 2.4, 0])
                        cube([5.4, 2.6, 8.5]);
                }
            }
            
      
        }
        
        //pi mounting holes
        translate([edge_offset - pi_offset, 0, height]) {
            //pi mounting offsets
            translate([0, (pi_width/2), -8])
                metric_thread(2.5, 0.45, 12, internal=true, test=DEVEL);
            translate([0, -(pi_width/2), -8])
                metric_thread(2.5, 0.45, 12, internal=true, test=DEVEL);
            translate([-pi_length, (pi_width/2), -8])
                metric_thread(2.5, 0.45, 12, internal=true, test=DEVEL);
            translate([-pi_length, -(pi_width/2), -8])
                metric_thread(2.5, 0.45, 12, internal=true, test=DEVEL);
        }

        //sensor mounting holes
        for(angle = [-45, 0, 45]) {
            offset = angle == 0 ? 5 : 3;
            rotate([0, 90, angle]) {
                translate([-height + offset, (sensor[0]/2), edge_offset - 8])
                    metric_thread(1.6, 0.35, 12, internal=true, test=DEVEL);
                translate([-height + offset, -(sensor[0]/2), edge_offset - 8])
                    metric_thread(1.6, 0.35, 12, internal=true, test=DEVEL);
                translate([-height + offset + sensor[1], (sensor[0]/2), edge_offset - 8])
                    metric_thread(1.6, 0.35, 12, internal=true, test=DEVEL);
                translate([-height + offset + sensor[1], -(sensor[0]/2), edge_offset - 8])
                    metric_thread(1.6, 0.35, 12, internal=true, test=DEVEL);
            }
        }
    }
}

//motor pegs
translate([-19.7, 0, motor[2] / 2]) {
    translate([0, (length/2) - motor[1], 0])
        rotate([-90, 0, 0])
            cylinder(d=6, h=1.9);
    translate([0, -(length/2) + motor[1], 0])
        rotate([90, 0, 0])
            cylinder(d=6, h=1.9);
}

//stabalizers
translate([edge_offset - stabalizer_r, 0, -stabalizer_r]) {
    cylinder(r = stabalizer_r, h = stabalizer_r);
    sphere(r = stabalizer_r);
}
translate([-edge_offset + stabalizer_r, 0, -stabalizer_r]) {
    cylinder(r = stabalizer_r, h = stabalizer_r);
    sphere(r = stabalizer_r);
}