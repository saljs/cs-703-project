# main.py
# Functions needed for high-level robot control and sensing,
# as well as main learning loop and data collection.

import argparse

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest='cmd', required=True)

parser_drl = subparsers.add_parser("drl", help="Train DRL agent")
parser_drl.add_argument("trials", type=int, help="Number of trials to run")
parser_drl.add_argument("save_dir", help="Directory to save to")

parser_pirl = subparsers.add_parser("pirl", help="Train PIRL agent")
parser_pirl.add_argument("sketch", help="File containing program sketch")
parser_pirl.add_argument("load_dir", help="Directory to load from")

parser_test = subparsers.add_parser("test", help="Test agents")
parser_test.add_argument("episodes", type=int, help="Number of episodes to test")
parser_test.add_argument("sketch", help="File containing program sketch")
parser_test.add_argument("load_dir", help="Directory to load from")

args = parser.parse_args()

from Robot import Robot
from ActorCritic import ActorCriticAgent
from PIRL import PIRLAgent

import numpy as np
import time, random, os

robot = Robot()
DRL_agent = ActorCriticAgent(robot)

def reset_robot():
    '''Resets the robot automatically for another episode'''
    # Move randomly
    robot.motors.set(random.uniform(0, 0.4),
                     random.uniform(0, 0.4), 0.25)
    # Move away from edges
    dist = robot.sensors.read()
    while (dist < 20).any():
        speed_l = -1 * (1 - (dist[0] / 170))
        speed_r = -1 * (1 - (dist[2] / 170))
        robot.motors.set(speed_l, speed_r, 0.25)
        dist = robot.sensors.read()
    # Find box in field of view
    direction = random.choice(((0.2, -0.2), (-0.2, 0.2)))
    while not robot.is_box_visible():
        robot.motors.set(*direction, 0.25)

def train_DRL(start):
    # Train the actor-critic model
    for trial in range(start, args.trials):
        reset_robot()
        reward, _ = DRL_agent.run_episode()
        with open(os.path.join(args.save_dir, "reward.csv"), "a") as logfile:
            logfile.write("{},{}\n".format(trial, reward))
        DRL_agent.model.save_weights(os.path.join(args.save_dir,
                                                  "model_wieghts"))
        print("Trial {}: reward = {}".format(trial, reward))

def synth_PIRL(agent):
    # Synthesize program
    last_reward = 0
    reward = 0
    last_program = None
    program = agent.program
    while True:
        print("Program candidate: {}".format(agent.get_string(program)))
        reset_robot()
        reward = agent.run_episode()[0]
        print("Reward: {}".format(reward))
        
        if last_reward > reward:
            break
        program = agent.update_program()
        agent.save(args.load_dir)
        last_reward = reward
        last_program = program
    print("Final program: {}".format(agent.get_string(last_program)))

def test_agent(agent, episodes):
    rewards = np.empty(episodes)
    for i in range(episodes):
        reset_robot()
        rewards[i] = agent.run_episode(False)[0]
    print("Test summary:")
    print("  Moved box in {} out of {} episodes."
            .format((rewards > 0).sum(), episodes))
    print("  Average reward: {}".format(rewards.mean()))
    print("  Average reward in episodes box was pushed: {}"
            .format((rewards > 0).mean()))

if args.cmd == "drl":
    # Create save directory
    if not os.path.exists(args.save_dir):
        os.mkdir(args.save_dir)
    start_trial = 0
    if os.path.exists(os.path.join(args.save_dir, "reward.csv")):
        with open(os.path.join(args.save_dir, "reward.csv"), "r") as logfile:
            start_trial = int(logfile.readlines()[-1].split(",")[0]) + 1
        DRL_agent.model.load_weights(os.path.join(args.save_dir,
                                                  "model_wieghts"))
    # Run training
    train_DRL(start_trial)

elif args.cmd == "pirl":
    DRL_agent.model.load_weights(os.path.join(args.load_dir, "model_wieghts"))
    sketch_file = open(args.sketch, "r")
    sketch = sketch_file.read()
    sketch_file.close()
    PIRL_agent = PIRLAgent(DRL_agent, sketch)
    if not PIRL_agent.load(args.load_dir):
        reset_robot()
        PIRL_agent.create_histories()
        print("Generating initial programs...")
        PIRL_agent.init_program(6)
        PIRL_agent.save(args.load_dir)
    # Run synthesis
    synth_PIRL(PIRL_agent)

elif args.cmd == "test":
    DRL_agent.model.load_weights(os.path.join(args.load_dir, "model_wieghts"))
    sketch_file = open(args.sketch, "r")
    sketch = sketch_file.read()
    sketch_file.close()
    PIRL_agent = PIRLAgent(DRL_agent, sketch)
    PIRL_agent.load(args.load_dir)
    
    print("Testing DRL agent...")
    test_agent(DRL_agent, args.episodes)

    print("\nTesting PIRL agent...")
    test_agent(PIRL_agent, args.episodes)
