# hardware.py
# Defines interfaces for controlling motors and reading from distance sensors
# and camera.

from PCA9685 import PCA9685
import RPi.GPIO as GPIO
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import numpy as np
import cv2

class Motors:
    # set channels
    RIGHT_PWM = 0
    RIGHT_IN1 = 1
    RIGHT_IN2 = 2
    LEFT_PWM = 5
    LEFT_IN1 = 3
    LEFT_IN2 = 4
    
    def __init__(self):
        self.pwm = PCA9685(0x40, debug=False)
        self.pwm.setPWMFreq(50)

    def set(self, left, right, duration=None):
        '''Sets motors to values (l, r), where l and r are floats
        between -1 and 1'''
        if left < -1 or left > 1 or right < -1 or right > 1:
            raise ValueError("motor values must be between -1 and 1")

        # determine speed (limit to half power)
        left_speed = int(abs(left) * 50)
        right_speed = int(abs(right) * 50)

        # set motor speeds
        self.pwm.setDutycycle(self.LEFT_PWM, left_speed)
        self.pwm.setDutycycle(self.RIGHT_PWM, right_speed)

        # set motor directions
        if left < 0:
            self.pwm.setLevel(self.LEFT_IN1, 1)
            self.pwm.setLevel(self.LEFT_IN2, 0)
        else:
            self.pwm.setLevel(self.LEFT_IN1, 0)
            self.pwm.setLevel(self.LEFT_IN2, 1)
        if right < 0:
            self.pwm.setLevel(self.RIGHT_IN1, 1)
            self.pwm.setLevel(self.RIGHT_IN2, 0)
        else:
            self.pwm.setLevel(self.RIGHT_IN1, 0)
            self.pwm.setLevel(self.RIGHT_IN2, 1)

        # go for duration if set
        if duration is not None:
            time.sleep(duration)
            self.stop()


    def stop(self):
        '''Stops all motors'''
        self.pwm.setDutycycle(self.LEFT_PWM, 0)
        self.pwm.setDutycycle(self.RIGHT_PWM, 0)

class Sensors:
    # set pins
    LEFT_T    = 5
    LEFT_E    = 6
    MIDDLE_T  = 23
    MIDDLE_E  = 24
    RIGHT_T   = 20
    RIGHT_E   = 21

    def __init__(self):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.LEFT_T, GPIO.OUT)
        GPIO.setup(self.MIDDLE_T, GPIO.OUT)
        GPIO.setup(self.RIGHT_T, GPIO.OUT)
        GPIO.setup(self.LEFT_E, GPIO.IN)
        GPIO.setup(self.MIDDLE_E, GPIO.IN)
        GPIO.setup(self.RIGHT_E, GPIO.IN)
        GPIO.output(self.LEFT_T, False)
        GPIO.output(self.MIDDLE_T, False)
        GPIO.output(self.RIGHT_T, False)

    def __del__(self):
        GPIO.cleanup()

    def read(self):
        '''Return a list of 3 sensor values in CM [left, middle, right]'''
        return np.array([self.__getReading(self.LEFT_T, self.LEFT_E),
                         self.__getReading(self.MIDDLE_T, self.MIDDLE_E),
                         self.__getReading(self.RIGHT_T, self.RIGHT_E)])

    def __getReading(self, trigger, echo):
        GPIO.output(trigger, False)
        time.sleep(0.00001)
        # set trigger to high
        GPIO.output(trigger, True)
        # set trigger after 0.01ms to low
        time.sleep(0.00001)
        GPIO.output(trigger, False)

        # record echo pulse
        end_time = time.time()
        start_time = time.time()
        while GPIO.input(echo) == 0 and start_time - end_time < 0.05:
            start_time = time.time()
        while GPIO.input(echo) == 1 and end_time - start_time < 0.05:
            end_time = time.time()

        dist = (end_time - start_time) * 17150.0
        if dist <= 3 or dist >= 450: dist = 0
        return dist

class Camera:
    def __iter__(self):
        self.camera = PiCamera()
        self.camera.resolution = (640, 480)
        self.camera.framerate = 32
        self.rawCapture = PiRGBArray(self.camera, size=(640, 480))
        self.frames = self.camera.capture_continuous(self.rawCapture, 
                                                     format="bgr",
                                                     use_video_port=True)
        return self

    def __next__(self):
        img = np.copy(next(self.frames).array)
        self.rawCapture.truncate(0)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.flip(img, 0)
        return img
